# Tutoriel de Mise à Jour de l'Application de Nana

| **Étape**                                     | **Commande/Action**                                                                                                                                                         |
|-----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **A. Prérequis**                              |                                                                                                                                                                             |
| Cloner le dépôt GitLab en local               | bash<br>git clone https://gitlab.com/votre-projet/nanaapp.git<br>                                                                                                      |
| Entrer dans le répertoire, supprimer `.git`, puis initialiser un nouveau dépôt Git | bash<br>cd nanaapp<br>rm -rf .git<br>git init<br>                                                                                                                      |
| Pousser le dépôt sur un serveur privé GitLab  | bash<br>git remote add origin https://gitlab.privé/votre-projet/nanaapp.git<br>git add .<br>git commit -m "Initial commit"<br>git push -u origin master<br>            |
| Télécharger et installer `make` en local      | bash<br>sudo apt update<br>sudo apt install -y make<br>                                                                                                               |
| Lancer `make`                                 | bash<br>make<br>                                                                                                                                                      |
| Déboguer les erreurs éventuelles.                                      |                                                                                                                                                                             |
| **B. Modifications à Apporter**               |                                                                                                                                                                             |
| **I. Modification de la phase de test**       |                                                                                                                                                                             |
| Modifier `nanaapp/.gitlab-ci.yml` | Nous devons modifier le before_script de run_tests pour installer les outils manquants.             | 
| |yaml<br>before_script:<br>  - apt update && apt install -y make python3-pip git python3.11-venv<br>                                                                   |
| Modifier `nanaapp/src/requirements.txt` | Nous devons modifier les versions de certaines dépendances et ajouter celles manquantes.      | 
| |plaintext<br>Flask==2.0.0<br>pytest==7.3.1<br>Jinja2==3.0.0<br>Werkzeug==2.0.0<br>                                                                                    |
| **II. Modification de la phase de build**     |                                                                                                                                                                             |
| Modifier `nanaapp/.gitlab-ci.yml` | Nous devons modifier les versions de build_image et adapter la commande Docker pour notre propre Docker Hub.            | 
| |yaml<br>stages:<br>  - build<br><br>build_image:<br>  image: docker:26.1.3<br>  services:<br>    - docker:26.1.3-dind<br><br>  script:<br>    - docker build -t meerak0le/cicd:tagname .<br>    - docker push meerak0le/cicd:tagname<br> |
| **III. Modification de la phase de déploiement** |                                                                                                                                                                             |
| Adapter `nanaapp/.gitlab-ci.yml` | Nous devons adapter la commande SSH et la commande Docker pour notre propre utilisation.             | 
| |yaml<br>stages:<br>  - deploy<br><br>deploy:<br>  script:<br>    - ssh -o StrictHostKeyChecking=no -i $SSH_KEY azureuser@20.199.87.23 "<br>        docker run -d -p 80:5000 meerak0le/cicd:tagname"<br> |
| **B. Modifications des Paramètres**           |                                                                                                                                                                             |
| Ajouter les variables dans les paramètres CI/CD | Dans les paramètres du projet, nous devons ajouter les différentes variables utilisées dans le fichier .gitlab-ci.yml. |
| | **Accéder aux paramètres CI/CD :**<br>Dans le menu déroulant à gauche du projet, allez dans **Settings** -> **CI/CD** -> **Variables** -> **Expand**.<br>**Ajouter les variables nécessaires :**<br>Cliquez sur "Add variable" et ajoutez les variables nécessaires, telles que `$SSH_KEY`(FILE), `$DOCKER_HUB_USERNAME`, et `$DOCKER_HUB_PASSWORD`. |

# Python Flask - Demo Web Application

This is a simple Python Flask web application. The app provides system information and a realtime monitoring screen with dials showing CPU, memory, IO and process information.

The app has been designed with cloud native demos & containers in mind, in order to provide a real working application for deployment, something more than "hello-world" but with the minimum of pre-reqs. It is not intended as a complete example of a fully functioning architecture or complex software design.

Typical uses would be deployment to Kubernetes, demos of Docker, CI/CD (build pipelines are provided), deployment to cloud (Azure) monitoring, auto-scaling

## Screenshot

![screen](https://user-images.githubusercontent.com/14982936/30533171-db17fccc-9c4f-11e7-8862-eb8c148fedea.png)

# Status

![](https://img.shields.io/github/last-commit/benc-uk/python-demoapp) ![](https://img.shields.io/github/release-date/benc-uk/python-demoapp) ![](https://img.shields.io/github/v/release/benc-uk/python-demoapp) ![](https://img.shields.io/github/commit-activity/y/benc-uk/python-demoapp)

Live instances:

[![](https://img.shields.io/website?label=Hosted%3A%20Azure%20App%20Service&up_message=online&url=https%3A%2F%2Fpython-demoapp.azurewebsites.net%2F)](https://python-demoapp.azurewebsites.net/)  
[![](https://img.shields.io/website?label=Hosted%3A%20Kubernetes&up_message=online&url=https%3A%2F%2Fpython-demoapp.kube.benco.io%2F)](https://python-demoapp.kube.benco.io/)

## Building & Running Locally

### Pre-reqs

- Be using Linux, WSL or MacOS, with bash, make etc
- [Python 3.8+](https://www.python.org/downloads/) - for running locally, linting, running tests etc
- [Docker](https://docs.docker.com/get-docker/) - for running as a container, or image build and push
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux) - for deployment to Azure

Clone the project to any directory where you do development work

```
git clone https://github.com/benc-uk/python-demoapp.git
```

### Makefile

A standard GNU Make file is provided to help with running and building locally.

```text
help                 💬 This help message
lint                 🔎 Lint & format, will not fix but sets exit code on error
lint-fix             📜 Lint & format, will try to fix errors and modify code
image                🔨 Build container image from Dockerfile
push                 📤 Push container image to registry
run                  🏃 Run the server locally using Python & Flask
deploy               🚀 Deploy to Azure Web App
undeploy             💀 Remove from Azure
test                 🎯 Unit tests for Flask app
test-report          🎯 Unit tests for Flask app (with report output)
test-api             🚦 Run integration API tests, server must be running
clean                🧹 Clean up project
```

Make file variables and default values, pass these in when calling `make`, e.g. `make image IMAGE_REPO=blah/foo`

| Makefile Variable | Default                |
| ----------------- | ---------------------- |
| IMAGE_REG         | ghcr<span>.</span>io   |
| IMAGE_REPO        | benc-uk/python-demoapp |
| IMAGE_TAG         | latest                 |
| AZURE_RES_GROUP   | temp-demoapps          |
| AZURE_REGION      | uksouth                |
| AZURE_SITE_NAME   | pythonapp-{git-sha}    |

The app runs under Flask and listens on port 5000 by default, this can be changed with the `PORT` environmental variable.

# Containers

Public container image is [available on GitHub Container Registry](https://github.com/users/benc-uk/packages/container/package/python-demoapp)

Run in a container with:

```bash
docker run --rm -it -p 5000:5000 ghcr.io/benc-uk/python-demoapp:latest
```

Should you want to build your own container, use `make image` and the above variables to customise the name & tag.

## Kubernetes

The app can easily be deployed to Kubernetes using Helm, see [deploy/kubernetes/readme.md](deploy/kubernetes/readme.md) for details

# GitHub Actions CI/CD

A working set of CI and CD release GitHub Actions workflows are provided `.github/workflows/`, automated builds are run in GitHub hosted runners

### [GitHub Actions](https://github.com/benc-uk/python-demoapp/actions)

[![](https://img.shields.io/github/workflow/status/benc-uk/python-demoapp/CI%20Build%20App)](https://github.com/benc-uk/python-demoapp/actions?query=workflow%3A%22CI+Build+App%22)

[![](https://img.shields.io/github/workflow/status/benc-uk/python-demoapp/CD%20Release%20-%20AKS?label=release-kubernetes)](https://github.com/benc-uk/python-demoapp/actions?query=workflow%3A%22CD+Release+-+AKS%22)

[![](https://img.shields.io/github/workflow/status/benc-uk/python-demoapp/CD%20Release%20-%20Webapp?label=release-azure)](https://github.com/benc-uk/python-demoapp/actions?query=workflow%3A%22CD+Release+-+Webapp%22)

[![](https://img.shields.io/github/last-commit/benc-uk/python-demoapp)](https://github.com/benc-uk/python-demoapp/commits/master)

## Running in Azure App Service (Linux)

If you want to deploy to an Azure Web App as a container (aka Linux Web App), a Bicep template is provided in the [deploy](deploy/) directory

For a super quick deployment, use `make deploy` which will deploy to a resource group, temp-demoapps and use the git ref to create a unique site name

```bash
make deploy
```

## Running in Azure App Service (Windows)

Just don't, it's awful 

