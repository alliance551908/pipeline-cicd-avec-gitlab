# Command Line Instructions

| Step | Command |
|------|---------|
| **1. Create a new repository** | git clone git@10.0.2.102:root/blank.git<br>cd blank<br>git switch --create main<br>touch README.md<br>git add README.md<br>git commit -m "add README"<br>git push --set-upstream origin main<br> |
| **2. Push an existing folder** | cd existing_folder<br>git init --initial-branch=main<br>git remote add origin git@10.0.2.102:root/blank.git<br>git add .<br>git commit -m "Initial commit"<br>git push --set-upstream origin main<br> |
| **3. Push an existing Git repository** | cd existing_repo<br>git remote rename origin old-origin<br>git remote add origin git@10.0.2.102:root/blank.git<br>git push --set-upstream origin --all<br>git push --set-upstream origin --tags<br> |
| **4. Upload existing files from your computer** | git config --global user.name "Administrator"<br>git config --global user.email "admin@example.com"<br> |

# Commandes Git

| Commande                             | Description                                                                                      |
|--------------------------------------|--------------------------------------------------------------------------------------------------|
| `git branch`                         | Liste toutes les branches locales dans le dépôt actuel.                                           |
| `git branch -D [nom_de_branche]`     | Supprime une branche locale de manière forcée. Utilisé lorsque vous voulez supprimer une branche même si elle n’a pas été fusionnée dans une autre branche. |
| `git branch -M [nouveau_nom_de_branche]` | Renomme la branche actuelle à `[nouveau_nom_de_branche]` de manière forcée.                      |
| `git checkout -b [nouvelle_branche] [branche_base]` | Crée une nouvelle branche nommée `[nouvelle_branche]` à partir de `[branche_base]` et bascule immédiatement sur cette nouvelle branche. |
| `git checkout [nom_de_branche]`      | Change la branche courante à `[nom_de_branche]`.                                                 |
| `git merge [nom_de_branche]`         | Fusionne la branche spécifiée `[nom_de_branche]` dans la branche courante.                        |
| `git merge [nom_de_branche] --allow-unrelated-histories` | Fusionne deux branches qui n'ont pas de base commune (utilisé lorsqu'on veut fusionner deux historiques de dépôt distincts). |
| `git rebase --continue`              | Continue un rebasage après avoir résolu des conflits. Utilisé après avoir corrigé les conflits et fait un `git add`. |
| `git rebase`                         | Applique les commits de la branche courante sur une autre branche, réécrivant l’historique pour obtenir un historique linéaire. Généralement suivi par une branche, par exemple `git rebase [branche]`. |
| `git log --oneline`                  | Affiche l’historique des commits sous une forme condensée (une ligne par commit).                 |
| `git log`                            | Affiche l’historique détaillé des commits.                                                       |
| `git remote show [nom_remote]`       | Affiche des informations sur le dépôt distant nommé `[nom_remote]`.                               |
| `git remote show`                    | Liste les dépôts distants configurés dans le dépôt actuel.                                        |
| `git fetch`                          | Récupère les objets et les références depuis un dépôt distant, mettant à jour les références locales (mais sans fusionner les changements dans votre branche courante). |
| `git rm --cached [fichier]`          | Supprime le fichier spécifié de l’index (c'est-à-dire de la prochaine validation) sans le supprimer du système de fichiers local. |
| `git rm -r --cached [dossier]`       | Supprime de façon récursive un dossier et son contenu de l’index sans le supprimer du système de fichiers local. |
| `git stash`                          | Sauvegarde les modifications en cours dans un stash (zone de stockage temporaire) et réinitialise l’arborescence de travail à l’état HEAD. |
| `git stash drop`                     | Supprime une entrée dans le stash. Par défaut, il supprime le stash le plus récent.               |
